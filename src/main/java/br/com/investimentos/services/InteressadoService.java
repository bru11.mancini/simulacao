package br.com.investimentos.services;

import br.com.investimentos.models.Interessado;
import br.com.investimentos.models.Produto;
import br.com.investimentos.repositories.InteressadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class InteressadoService {

    @Autowired
    private InteressadoRepository interessadoRepository;

    @Autowired
    private ProdutoService produtoService;

    public Interessado efetuarSimulacao(int idProduto, Interessado interessado){
        Produto produto = produtoService.buscaPorIdProduto(idProduto);

        interessado.setProdutos(produto);
        Interessado interessadoObjeto = interessadoRepository.save(interessado);
        return interessadoObjeto;

        }

    public Interessado buscarPorIdInteressado(int idInteressado){
        Optional<Interessado> optionalInteressado = interessadoRepository.findById(idInteressado);
        if (optionalInteressado.isPresent()){
            return optionalInteressado.get();
        }
        throw new RuntimeException("O interessado não foi encontrado");
    }

    public Iterable<Interessado> interessados(){
        Iterable<Interessado> interessados = interessadoRepository.findAll();
        return interessados;
    }

    public Interessado atualizarInteressado(int idInteressado, Interessado interessado){
        if (interessadoRepository.existsById(idInteressado)){
            interessado.setIdInteressado(idInteressado);
            Interessado interessadoObjeto = efetuarSimulacao(idInteressado, interessado);
            return interessadoObjeto;
        }
        throw new RuntimeException("Não foi possível a atualização");
    }

    public void deletarInteressado(int idInteressado){
        if (interessadoRepository.existsById(idInteressado)){
            interessadoRepository.deleteById(idInteressado);
        } else {
            throw new RuntimeException("O interessado não pôde ser deletado");
        }
    }
}
