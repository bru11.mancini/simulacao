package br.com.investimentos.services;

import br.com.investimentos.models.Produto;
import br.com.investimentos.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto buscaPorIdProduto(int idProduto){
        Optional<Produto> optionalProduto = produtoRepository.findById(idProduto);
        if (optionalProduto.isPresent()){
            return optionalProduto.get();
        }
        throw new RuntimeException("O produto não foi encontrado na consulta");
    }

    public Produto atualizarProduto (int idProduto, Produto produto){
        if (produtoRepository.existsById(idProduto)){
            produto.setIdProduto(idProduto);
            Produto produtoObjeto = salvarProduto(produto);
            return produtoObjeto;
        }
        throw new RuntimeException("O produto não foi encontrado para ser atualizado");
    }

    public void deletarProduto (int idProduto){
        if (produtoRepository.existsById(idProduto)){
            produtoRepository.deleteById(idProduto);
        }
    }

    public Iterable<Produto> buscarTodosOsProdutos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public List<Produto> buscarPorTodosOsId(List<Integer> idProduto){
        Iterable<Produto> produto = produtoRepository.findAllById(idProduto);
        return (List) produto;
    }

}
