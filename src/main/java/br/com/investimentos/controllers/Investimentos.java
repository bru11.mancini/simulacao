package br.com.investimentos.controllers;

import br.com.investimentos.dto.RespostaDTO;
import br.com.investimentos.models.Interessado;
import br.com.investimentos.models.Produto;
import br.com.investimentos.services.InteressadoService;
import br.com.investimentos.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class Investimentos {

    @Autowired
    private InteressadoService interessadoService;

    @Autowired
    private ProdutoService produtoService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto registrarProduto(@RequestBody Produto produto){
        return produtoService.salvarProduto(produto);
    }


    @PostMapping("/{idProduto}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    //public Interessado registrarInteressado(@PathVariable(name = "idProduto") int idProduto, @RequestBody @Valid Interessado interessado){
    public RespostaDTO registrarInteressado(@PathVariable(name = "idProduto") int idProduto, @RequestBody @Valid Interessado interessado){
        try {
            Interessado interessadoObjeto = interessadoService.efetuarSimulacao(idProduto, interessado);
            RespostaDTO respostaDTO = new RespostaDTO(interessadoObjeto.getProdutos(), interessadoObjeto);
            return  respostaDTO;

            //return interessadoObjeto;


        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Produto> exibirTodos (@RequestParam(name = "nomeDoProduto", required = false) String nomeProduto) {
        if (nomeProduto != null){
            Iterable<Produto> produtos = produtoService.buscarTodosOsProdutos();
            return produtos;
        }
        Iterable<Produto> produtos = produtoService.buscarTodosOsProdutos();
        return produtos;
    }




}
