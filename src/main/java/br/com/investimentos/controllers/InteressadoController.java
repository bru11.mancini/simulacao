package br.com.investimentos.controllers;

import br.com.investimentos.dto.RespostaDTO;
import br.com.investimentos.models.Interessado;
import br.com.investimentos.models.Produto;
import br.com.investimentos.services.InteressadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacoes")
public class InteressadoController {

    @Autowired
    private InteressadoService interessadoService;



    @GetMapping
    //public Iterable<Interessado> exibirTodosInteressados (@RequestParam(name = "idInteressado", required = false) int idInteressado){
    public Iterable<Interessado> exibirTodosInteressados (){

        Iterable<Interessado> interessados = interessadoService.interessados();
        return interessados;

    }

    @GetMapping("/{idInteressado}")
    @ResponseStatus(HttpStatus.OK)
    public Interessado buscarPorIdInteressado(@PathVariable(name = "idInteressado") int idInteressado){
        try {
            Interessado interessado = interessadoService.buscarPorIdInteressado(idInteressado);

            return interessado;

        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }



}
