package br.com.investimentos.repositories;

import br.com.investimentos.models.Interessado;
import org.springframework.data.repository.CrudRepository;

public interface InteressadoRepository extends CrudRepository<Interessado, Integer> {

}
