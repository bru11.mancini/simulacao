package br.com.investimentos.dto;

import br.com.investimentos.models.Interessado;
import br.com.investimentos.models.Produto;

import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class RespostaDTO {
    private double rendimentoPorMes;

    @Digits(integer = 8, fraction = 2)
    private double montanteAcumulado;


    public RespostaDTO(Produto produto, Interessado interessado) {

        montanteAcumulado = interessado.getValorAplicacaoInteressado();
        rendimentoPorMes = produto.getRendimento();

        for (int i = 1; i <= interessado.getQtdMesesAplicacaoInteressado(); i++){
            montanteAcumulado += montanteAcumulado * (produto.getRendimento() / 100);
        }
        BigDecimal bd = new BigDecimal(montanteAcumulado).setScale(2, RoundingMode.FLOOR);
        this.montanteAcumulado = bd.doubleValue();

    }

    //public RespostaDTO() {
    //}

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontanteAcumulado() {
        return montanteAcumulado;
    }

    public void setMontanteAcumulado(double montanteAcumulado) {
        BigDecimal bd = new BigDecimal(montanteAcumulado).setScale(2, RoundingMode.FLOOR);
        this.montanteAcumulado = bd.doubleValue();
    }

}
