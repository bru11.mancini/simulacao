package br.com.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import java.util.List;

@Entity
public class Interessado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idInteressado;

    private String nomeInteressado;

    @Email(message = "O formato do email está bem errado")
    private String emailInteressado;

    @DecimalMin(value = "0.1", message = "Tudo bem")
    private Double valorAplicacaoInteressado;

    private int    qtdMesesAplicacaoInteressado;

    public Interessado() {
    }

    public int getIdInteressado() {
        return idInteressado;
    }

    public void setIdInteressado(int idInteressado) {
        this.idInteressado = idInteressado;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmailInteressado() {
        return emailInteressado;
    }

    public void setEmailInteressado(String emailInteressado) {
        this.emailInteressado = emailInteressado;
    }

    public Double getValorAplicacaoInteressado() {
        return valorAplicacaoInteressado;
    }

    public void setValorAplicacaoInteressado(Double valorAplicacaoInteressado) {
        this.valorAplicacaoInteressado = valorAplicacaoInteressado;
    }

    public int getQtdMesesAplicacaoInteressado() {
        return qtdMesesAplicacaoInteressado;
    }

    public void setQtdMesesAplicacaoInteressado(int qtdMesesAplicacaoInteressado) {
        this.qtdMesesAplicacaoInteressado = qtdMesesAplicacaoInteressado;
    }

    //Abaixo é o relacionamento e o que preciso dele.

    @ManyToOne(cascade = CascadeType.ALL)
    private Produto produto;


    public Produto getProdutos(){
        return produto;
    }

    public void setProdutos(Produto produto){
        this.produto = produto;
    }



}
