Consultar Produtos
    localhost:8080/investimentos/
    
=====================================

Verificar todas simulações
    localhost:8080/simulacoes/

=====================================


Inserir Produto
    localhost:8080/investimentos/
{
    "nome":"Poupança Supe2r hi222pe2r meg4a",
    "rendimento": "2.25"
}
=====================================

Inserir simulação
localhost:8080/investimentos/6/simulacao    // o 6 é o produto

{
    "nomeInteressado":"Jose da Silva",
    "emailInteressado": "jose@jose",
    "valorAplicacaoInteressado": "1000.00",
    "qtdMesesAplicacaoInteressado": 3
}

